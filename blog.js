  //blog

  var pageType = "";

  if (document.location.pathname === "/" || document.location.pathname.includes("/page")) {
      pageType = "Blog Home Page";
  } else if (document.location.pathname.includes("/category/")) {
      pageType = "Blog Article Category Page";
  } else if (/\/\d{4}\/\d{2}\//.test(document.location.pathname)) {
      pageType = "MonthWise Articles Pages";
  } else {
      pageType = "Blog Article Page";
  }

  pageType = pageType.toLowerCase();

  function topNavDL(a) {
      var ele = "";
      ele = a.innerText;

      dataLayer.push({
          'event': 'top_navigation_menu_click_All_Pages_1',
          'PageType': pageType,
          'top_element': ele.toLowerCase()
      });
  }

  function blogLogoDL(a) {
      dataLayer.push({
          'event': 'logo_click_All_Pages_2',
          'PageType': pageType
      });
  }

  function socialIconDL(a) {
      var ele = "";
      ele = a.title;

      dataLayer.push({
          'event': 'social_icons_click_All_Pages_3',
          'PageType': pageType,
          'social_element': ele.toLowerCase()
      });
  }

  function mainNavDL(a) {
      var levelNumber = "";
      var ln = 1;
      var fullPathofMenu = [];
      var fullPathofMenuStr = "";
      var els = [];

      fullPathofMenu.push(a.innerText);

      while (a) {
          if (a.id == "main-nav") break;
          els.unshift(a);
          a = a.parentNode;
      }

      for (var i = els.length - 1; i >= 0; i--) {
          a = els[i];
          if (a.nodeName == "UL") {
              if (a.previousSibling.previousSibling.innerText != undefined) {
                  fullPathofMenu.push(a.previousSibling.previousSibling.innerText);
              }

              if (a.className == "sub-menu") {
                  ln++;
              }
          }
      }

      combinedStr = "";
      for (var i = fullPathofMenu.length - 1; i >= 0; i--) {
          combinedStr += fullPathofMenu[i] + " | ";
      }

      fullPathofMenuStr = combinedStr.substring(0, combinedStr.length - 3);
      levelNumber = ln;


      dataLayer.push({
          'event': 'main_navigation_menu_click_All_Pages_4',
          'PageType': pageType,
          'number_level_main_navigation_click': levelNumber,
          'menu_element': fullPathofMenuStr.toLowerCase()
      });
  }

  function searchTermDL(a) {
      var ele = "";

      var inputEles = document.querySelectorAll(".searchform input");
      if (inputEles.length > 0) {
          for (var i = 0; i < inputEles.length; i++) {
              if (inputEles[i].type == "text") {
                  ele = inputEles[i].value;
              }
          }
      }
      dataLayer.push({
          'event': 'search_action_All_Pages_5',
          'PageType': pageType,
          'search_query_element': ele.toLowerCase()
      });
  }

  function emailSubDL(a) {
      dataLayer.push({
          'event': 'blog_subscribe_click_All_Pages_6',
          'PageType': pageType
      });
  }

  function sideBlogListDL(a) {
      var widget_type = "";
      var page_number = "1";
      var link_type = "";
      var article_title = "";

      if (a.parentNode.className.includes("thumbnail")) {
          link_type = "image";
      }
      if (a.parentNode.className.includes("entry-title")) {
          link_type = "text";
      }

      var selectedTab = document.querySelectorAll(".sidebar .widget.widget_wpt ul.wpt-tabs .selected");
      if (selectedTab.length > 0) {
          widget_type = selectedTab[0].innerText.toLowerCase();
      }

      if (widget_type === "recent") {
          var pageNumele = document.querySelectorAll("#recent-tab-content > input");
          if (pageNumele.length > 0) {
              page_number = pageNumele[0].value;
          }
      } else {
          var pageNumele = document.querySelectorAll("#popular-tab-content > input");
          if (pageNumele.length > 0) {
              page_number = pageNumele[0].value;
          }
      }


      var article_title = a.innerText;

      if (article_title.trim() == "") {
          article_title = a.parentNode.nextElementSibling.querySelectorAll("a");
          if (article_title.length > 0) {
              article_title = article_title[0].innerText;
          }
      }

      dataLayer.push({
          'event': 'widget_click_All_Pages_7',
          'PageType': pageType,
          'widget_type': widget_type,
          'page_number': page_number,
          'link_type': link_type,
          'article_title': article_title.toLowerCase()
      });
  }

  function archivesClickDL(a) {
      var ele = "";
      ele = a.innerText.toLowerCase();
      dataLayer.push({
          'event': 'archives_click_All_Pages_8',
          'PageType': pageType,
          'element': ele
      });
  }

  function scrollTopClickDL(a) {
      var pageTitle = "";
      pageTitle = document.querySelectorAll("head > title");
      if (pageTitle.length > 0) {
          pageTitle = pageTitle[0].innerText;
      }
      dataLayer.push({
          'event': 'scroll_back_to_top_click_All_Pages_9',
          'PageType': pageType,
          'page_title': pageTitle.toLowerCase()
      });
  }

  function homeCarouselDL(a) {
      var ele = a.innerText;
      var bannerNumber = 1;
      var bannerText = "";

      var parentContainer = a.closest(".frs-slide-img");
      if (parentContainer) {
          bannerNumber = getElementIndex(parentContainer);
      }

      var bannertextContainer = a.closest(".frs-caption-content");
      if (bannertextContainer) {
          bannerText = bannertextContainer.querySelectorAll("h4");
          if (bannerText.length > 0) {
              bannerText = bannerText[0].innerText;
          } else {
              bannerText = "";
          }
      }

      dataLayer.push({
          'event': 'banner_click_Blog_HomePage_1',
          'PageType': pageType,
          'element': ele.toLowerCase(),
          'banner_number': bannerNumber,
          'active_banner_text': bannerText.toLowerCase()
      });
  }

  function homeBlogsDL(a) {
      var linkType = "";
      var pageNumber = 1;
      var articleTitle = "";
      var categoryName = "";

      pageNumber = document.querySelectorAll("#wp-pagenavibox span.current");
      if (pageNumber.length > 0) {
          pageNumber = pageNumber[0].innerText;
      }
      if (a.parentNode.nodeName.toLowerCase() === "h1") {
          linkType = "text";

          articleTitle = a.innerText.toLowerCase();

      } else if (a.parentNode.className.includes("post-thumbnail")) {

          linkType = "image";
          articleTitle = a.closest("article");
          if (articleTitle) {
              articleTitle = articleTitle.querySelectorAll("h1.title");
              if (articleTitle.length > 0) {
                  articleTitle = articleTitle[0].innerText;
              }
          }
      }

      categoryEle = a.closest("article");
      if (categoryEle) {
          categoryEle = categoryEle.querySelectorAll(".entry-meta .icon-folder");
          if (categoryEle.length > 0) {
              categoryName = categoryEle[0].nextElementSibling.innerText;
          }
      }


      dataLayer.push({
          'event': 'articles_grid_click_Blog_HomePage_2',
          'PageType': pageType,
          'link_type': linkType,
          'page_number': pageNumber,
          'article_title': articleTitle.toLowerCase(),
          'category_name': categoryName.toLowerCase()
      });
  }

  function homePagerDL(a) {
      var pageNumber = 1;

      pageNumber = document.querySelectorAll("#wp-pagenavibox span.current");
      if (pageNumber.length > 0) {
          pageNumber = pageNumber[0].innerText;
      }

      pageNumber = pageNumber + " > " + a.innerText;

      dataLayer.push({
          'event': 'bottom_page_navigation_click_Blog_HomePage_3',
          'PageType': pageType,
          'page_number': pageNumber
      });
  }

  function homeBlogCateDL(a) {
      var categoryName = "";
      categoryName = a.innerText.toLowerCase();

      dataLayer.push({
          'event': 'category_folder_click_Blog_HomePage_4',
          'PageType': pageType,
          'category_name': categoryName
      });
  }

  function articleDetailLinkDL(a) {
      var ele = "";
      ele = a.innerText.toLowerCase();
      var nextLink = "";
      if (a.href != undefined && a.href != undefined) {
          nextLink = a.href.toLowerCase();
      }

      dataLayer.push({
          'event': 'article_details_click_Blog_Article_Page_1',
          'PageType': pageType,
          'element': ele,
          'nextLink': nextLink
      });
  }

  function articleApplyOnlineDL(a) {
      var buttonType = "";
      var ele = "";
      var categoryName = "";

      if (a.getAttribute('data-toggle') !== undefined && a.getAttribute('data-toggle') !== null) {
          buttonType = "collapse";
      } else {
          buttonType = "submit"
      }

      ele = document.querySelectorAll(".wrapper .content .posFixed h2");
      if (ele.length > 0) {
          ele = ele[0].innerText.toLowerCase() + " | ";
      }

      ele += a.innerText.toLowerCase();
      categoryName = document.querySelectorAll("#post div.entry-meta > ul > li:nth-child(5) > span");
      if (categoryName.length > 0) {
          categoryName = categoryName[0].innerText;
      }

      dataLayer.push({
          'event': 'click_to_action_click_Blog_Article_Page_2',
          'PageType': pageType,
          'button_type': buttonType,
          'element': ele,
          'category_name': categoryName.toLowerCase()
      });
  }

  function articleSubmitDL(a) {
      var buttonType = "";
      var ele = "";
      var categoryName = "";

      if (a.getAttribute('data-toggle') !== undefined && a.getAttribute('data-toggle') !== null) {
          buttonType = "collapse";
      } else {
          buttonType = "submit";
          var err = a.closest("form");
          if (err) {
              err = err.querySelectorAll("form .form-group .errorMsg");
              if (err.length > 0) {
                  for (var i = 0; i < err.length; i++) {
                      if (err[i].style.display != "none") {
                          return;
                      }
                  }
              }
          }
      }

      ele = document.querySelectorAll(".wrapper .content .posFixed h2");
      if (ele.length > 0) {
          ele = ele[0].innerText.toLowerCase() + " | ";
      }



      ele += a.innerText.toLowerCase();
      categoryName = document.querySelectorAll("#post div.entry-meta > ul > li:nth-child(5) > span");
      if (categoryName.length > 0) {
          categoryName = categoryName[0].innerText;
      }

      dataLayer.push({
          'event': 'click_to_action_click_Blog_Article_Page_2',
          'PageType': pageType,
          'button_type': buttonType,
          'element': ele,
          'category_name': categoryName.toLowerCase()
      });
  }

  function bottomArticleBlogClickDL(a) {
      var linkType = "";
      var articleTitle = "";
      if (a.parentNode.className.includes("image-slider")) {
          linkType = "image";
          articleTitle = a.parentNode.nextElementSibling.querySelectorAll("a");
          if (articleTitle.length > 0) {
              articleTitle = articleTitle[0].innerText.toLowerCase();
          }
      }
      if (a.parentNode.className.includes("title-slider")) {
          linkType = "text"
          articleTitle = a.innerText.toLowerCase();
      }

      dataLayer.push({
          'event': 'bottom_article_grid_click_Blog_Article_Page_3',
          'PageType': pageType,
          'link_type': linkType,
          'article_title': articleTitle
      });
  }

  function calcClickDL(a) {
      var elem = a.innerText;
      if (elem != "") {
          elem = "Calculators | " + elem;
      }

      dataLayer.push({
          'event': 'right_sidebar_click_Blog_Article_Page_4',
          'PageType': pageType,
          'element': elem.toLowerCase()
      });
  }

  function getElementIndex(node) {
      var prop = document.body.previousElementSibling ? 'previousElementSibling' : 'previousSibling';
      var i = 1;
      while (node = node[prop]) {++i }
      return i;
  }

  function bindLinkElements(selector, actionFunction) {

      var elem = document.querySelectorAll(selector);

      if (elem.length > 0) {
          var i = 0;
          for (i = 0; i < elem.length; i++) {
              elem[i].onclick = function() {
                  actionFunction(this);
                  return true;
              };
          }
      }
  }

  function bindBlogs(a) {

      widgetTabClick(a);
      var sideBlogInterval = setInterval(function() {
          var psideBarBlogs = document.querySelectorAll("#popular-tab-content a");
          var rsideBarBlogs = document.querySelectorAll("#recent-tab-content a");

          if (psideBarBlogs.length > 0 || rsideBarBlogs.length > 0) {
              clearInterval(sideBlogInterval);
              bindLinkElements("#popular-tab-content a, #recent-tab-content a", sideBlogListDL);
          }

          var blogNavs = document.querySelectorAll(" div.wpt-pagination > a");
          if (blogNavs.length > 0) {
              bindLinkElements("div.wpt-pagination > a", bindBlogs);
          }
      }, 2000);
  }


  function widgetTabClick(a) {
      var widget_type = "";
      var page_number = "1";
      var selectedTab = document.querySelectorAll(".sidebar .widget.widget_wpt ul.wpt-tabs .selected");
      if (selectedTab.length > 0) {
          widget_type = selectedTab[0].innerText.toLowerCase();
      }

      if (widget_type.trim().toLowerCase() == "recent") {
          var pageNumele = document.querySelectorAll("#recent-tab-content > input");
          if (pageNumele.length > 0) {
              page_number = pageNumele[0].value;
          }
      } else {
          var pageNumele = document.querySelectorAll("#popular-tab-content > input");
          if (pageNumele.length > 0) {
              page_number = pageNumele[0].value;
          }
      }

      dataLayer.push({
          'event': 'widget_click_All_Pages_7',
          'PageType': pageType,
          'widget_type': widget_type,
          'page_number': page_number,
          'link_type': "text",
          'article_title': a.innerText.toLowerCase()

      });
  }

  (function() {
      var topNavInterval = setInterval(function() {
          if (document.readyState === 'complete') {
              clearInterval(topNavInterval);
          }
          var topNav = document.querySelectorAll("#top-nav a");
          if (topNav.length > 0) {
              clearInterval(topNavInterval);

              // top_navigation_menu_click_All_Pages_1
              bindLinkElements("#top-nav a", topNavDL);
              // logo_click_All_Pages_2
              bindLinkElements("div.header > div.container div.logo a", blogLogoDL);

              // social_icons_click_All_Pages_3
              bindLinkElements("div.icons-social a", socialIconDL);

              // main_navigation_menu_click_All_Pages_4
              bindLinkElements("#navigation #main-nav a", mainNavDL);

              // search_action_All_Pages_5
              bindLinkElements(".searchform #searchsubmit_search", searchTermDL);

              // blog_subscribe_click_All_Pages_6
              bindLinkElements(".eemail_widget #eemail_txt_Button", emailSubDL);
          }
      }, 2000);

      var sideBlogInterval = setInterval(function() {
          if (document.readyState === 'complete') {
              clearInterval(sideBlogInterval);
          }
          var sideBarBlogs = document.querySelectorAll(".sidebar .widget.widget_wpt .inside ul a");
          if (sideBarBlogs.length > 0) {
              clearInterval(sideBlogInterval);
              // widget_click_All_Pages_7
              bindLinkElements(".sidebar .widget.widget_wpt .inside ul a", sideBlogListDL);
              bindLinkElements("#recent-tab, #popular-tab", bindBlogs);
          }

          var blogNavs = document.querySelectorAll(" div.wpt-pagination > a");
          if (blogNavs.length > 0) {
              bindLinkElements("div.wpt-pagination > a", bindBlogs);
          }
      }, 2000);

      var sideArchiesInterval = setInterval(function() {
          if (document.readyState === 'complete') {
              clearInterval(sideArchiesInterval);
          }
          var sideArchies = document.querySelectorAll(".widget_archive ul li a");
          if (sideArchies.length > 0) {
              clearInterval(sideArchiesInterval);
              bindLinkElements(".widget_archive ul li a", archivesClickDL);
          }
      }, 2000);

      var scrollTopInterval = setInterval(function() {
          if (document.readyState === 'complete') {
              clearInterval(scrollTopInterval);
          }
          var scrollWidget = document.querySelectorAll("body > div.scroll-back-to-top-wrapper");
          if (scrollWidget.length > 0) {
              clearInterval(sideArchiesInterval);
              bindLinkElements("body > div.scroll-back-to-top-wrapper", scrollTopClickDL);
          }
      }, 2000);


      if (pageType === "blog home page" || pageType === "monthwise articles pages" || pageType === "blog article category page") {
          var maxTime = 10000;
          var time = 0;

          var homeCarouselInternal = setInterval(function() {
              if (document.readyState === 'complete') {
                  clearInterval(homeCarouselInternal);
              }
              var homeCarousels = document.querySelectorAll("#captionhomepjc > div > div > div > p > a");
              if (homeCarousels.length > 0) {
                  clearInterval(homeCarouselInternal);
                  bindLinkElements("#captionhomepjc > div > div > div > p > a", homeCarouselDL);
              } else {
                  if (time > maxTime) {
                      clearInterval(homeCarouselInternal);
                      return;
                  }
                  time += 100;
              }
          }, 2000);

          var homeBlogInterval = setInterval(function() {
              if (document.readyState === 'complete') {
                  clearInterval(homeBlogInterval);
              }
              var homeBlogs = document.querySelectorAll(".col-content article h1 a");
              if (homeBlogs.length > 0) {
                  clearInterval(homeBlogInterval);
                  bindLinkElements(".col-content article h1 a", homeBlogsDL);
                  bindLinkElements(".col-content .post-thumbnail a", homeBlogsDL);
                  bindLinkElements("#wp-pagenavibox div  a", homePagerDL);
                  bindLinkElements(".col-content .entry-meta > ul > li > span > a", homeBlogCateDL);

              }
          }, 2000);
      }


      if (pageType === "blog article page") {
          // article_details_click_Blog_Article_Page_1
          var articleDetailLinkInterval = setInterval(function() {
              if (document.readyState === 'complete') {
                  clearInterval(articleDetailLinkInterval);
              }
              var articleDetailLinks = document.querySelectorAll("#post div.post-content a");
              if (articleDetailLinks.length > 0) {
                  clearInterval(articleDetailLinkInterval);
                  bindLinkElements("#post div.post-content a", articleDetailLinkDL);
              }
          }, 2000);


          // click_to_action_click_Blog_Article_Page_2
          var articleApplyOnlineInterval = setInterval(function() {
              if (document.readyState === 'complete') {
                  clearInterval(articleApplyOnlineInterval);
              }
              var articleApplyEles = document.querySelectorAll(".wrapper .content .posFixed h3  a");
              if (articleApplyEles.length > 0) {
                  clearInterval(articleApplyOnlineInterval);
                  bindLinkElements(".wrapper .content .posFixed h3  a", articleApplyOnlineDL);
                  bindLinkElements(".formSide .Submit", articleSubmitDL);
              }
          }, 2000);


          // bottom_article_grid_click_Blog_Article_Page_3
          var bottomArticleInterval = setInterval(function() {
              if (document.readyState === 'complete') {
                  clearInterval(bottomArticleInterval);
              }
              var bottomArticles = document.querySelectorAll(".under-slider-block  div.title-slider > a");
              if (bottomArticles.length > 0) {
                  clearInterval(bottomArticleInterval);
                  bindLinkElements(".under-slider-block  div.title-slider > a", bottomArticleBlogClickDL);
                  bindLinkElements(".under-slider-block div.bx-viewport div.image-slider > a", bottomArticleBlogClickDL);

              }
          }, 2000);

          var calcInterval = setInterval(function() {
              if (document.readyState === 'complete') {
                  clearInterval(calcInterval);
              }
              var rightSideCalc = document.querySelectorAll("#Calculators a");
              if (rightSideCalc.length > 0) {
                  clearInterval(calcInterval);
                  bindLinkElements("#Calculators a", calcClickDL);
              }

          }, 2000);

      }

  })();